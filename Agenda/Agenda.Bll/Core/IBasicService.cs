﻿using System.Collections.Generic;

namespace Agenda.Bll.Core
{
    public interface IBasicService<TRepository, TBo>
        where TRepository : class
        where TBo : class 
    {
        TRepository Repository { get; set; }

        IEnumerable<TBo> All();
        TBo Get(int id);

        int CreateOrUpdate(TBo item);
        bool Delete(int id);
    }
}
