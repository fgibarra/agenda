﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Agenda.Bo;
using Agenda.Dal.Repositories;

namespace Agenda.Bll.Core
{
    interface ICitasService
    {        
        CitasRepository CitasRepository { get; set; }

        IEnumerable<BoCita> GetCitas();
        BoCita GetCita(int id );

        int CreateOrUpdateCita(BoCita item);
        bool DeleteCita(int id);



    }
}
