﻿using System.Collections.Generic;
using Agenda.Bll.Core;
using Agenda.Bo;
using Agenda.Dal.Repositories;

namespace Agenda.Bll.Services
{
    public class ProductosService: IBasicService<ProductosRepository, BoProducto>
    {
        public ProductosRepository Repository { get; set; }

        public ProductosService()
        {
            Repository = new ProductosRepository();
        }

        public IEnumerable<BoProducto> All()
        {
            return Repository.All();
        }

        public BoProducto Get(int id)
        {
            return Repository.Get(id);
        }

        public int CreateOrUpdate(BoProducto item)
        {
            return Repository.CreateOrUpdate(item);
        }

        public bool Delete(int id)
        {
            return Repository.Delete(id);
        }
    }
}
