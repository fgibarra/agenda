﻿using System.Collections.Generic;
using Agenda.Bll.Core;
  using Agenda.Bo;
using Agenda.Dal.Repositories;

namespace Agenda.Bll.Services
{
    public class CitasService: ICitasService
    {        
        public CitasRepository CitasRepository { get; set; }

        public CitasService()
        {                        
            CitasRepository = new CitasRepository();
        }
        

        public IEnumerable<BoCita> GetCitas()
        {
            return  CitasRepository.All();
        }

        public BoCita GetCita(int id)
        {
            return CitasRepository.Get(id);
        }

        public int CreateOrUpdateCita(BoCita item)
        {
            return CitasRepository.CreateOrUpdate(item);
        }

        public bool DeleteCita(int id)
        {
            return CitasRepository.Delete(id);
        }
    }
}
