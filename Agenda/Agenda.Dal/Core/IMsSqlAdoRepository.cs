﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Agenda.Dal.Core
{
    public interface IMsSqlAdoRepository<TBo> 
        where TBo :class
    {
        bool HasError { get; set; }

        string ErrorMessage { get; set; }

        TBo Get(int id);

        IEnumerable<TBo> All();

        int CreateOrUpdate(TBo item);

        bool Delete(int id);

        IEnumerable<TBo> PopulateRecords(List<Object[]> listOfValues);

        SqlCommand CreateGetAllQuery();
        SqlCommand CreateInsertOrUpdateQuery(TBo item);
        SqlCommand CreateGetByIdQuery(int id);
        SqlCommand CreateDeleteQuery(int id);
        SqlConnection Connection { get; set; }

        ModoEjecucion ModoEjecucion { get; set; }

        void InitConnection();
        void FinalizeConnection();
    }
}
