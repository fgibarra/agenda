﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Dal.Core
{
    public enum ModoEjecucion
    {
        Single = 1,
        UnitOfWork = 2
    }
}
