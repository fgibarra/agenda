﻿using System.Configuration;
using System.Data.SqlClient;

namespace Agenda.Dal.Helpers
{
    public  class DatabaseHelper
    {
        public static SqlConnection GetConnection()
        {
            return new SqlConnection(
               ConfigurationManager.ConnectionStrings["AgendaContext"].ConnectionString);
        }
    }
}
