﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Agenda.Bo;
using Agenda.Dal.Core;

namespace Agenda.Dal.Repositories
{
    public class ProductosRepository : RepositoryBase<BoProducto>
    {
        public ProductosRepository()
        {
            ModoEjecucion = ModoEjecucion.Single;
        }

        public ProductosRepository(SqlConnection connection)
        {
            Connection = connection;
            ModoEjecucion = ModoEjecucion.UnitOfWork;
        }

        public override IEnumerable<BoProducto> PopulateRecords(List<object[]> listOfValues)
        {
            var lista = listOfValues.Select(values => new BoProducto
            {
                IdProducto = int.Parse(values[0].ToString()),
                Nombre = values[1].ToString(),
                Descripcion = values[2].ToString(),
                Precio = decimal.Parse(values[3].ToString())
            }).ToList();

            return lista;
        }

        public override SqlCommand CreateGetAllQuery()
        {
            var allCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Listar_Productos",
                CommandType = CommandType.StoredProcedure
            };
            allCommand.Parameters.Add("p_id_producto", SqlDbType.Int).Value = 0;

            return allCommand;
        }

        public override SqlCommand CreateInsertOrUpdateQuery(BoProducto item)
        {
            var creaInsCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Crear_Actualizar_Productos",
                CommandType = CommandType.StoredProcedure
            };
            creaInsCommand.Parameters.Add("p_id_producto", SqlDbType.Int).Value = item.IdProducto;
            creaInsCommand.Parameters.Add("p_nombre", SqlDbType.VarChar, 50).Value = item.Nombre;
            creaInsCommand.Parameters.Add("p_descripcion", SqlDbType.VarChar, 50).Value = item.Descripcion;
            creaInsCommand.Parameters.Add("p_precio", SqlDbType.Float).Value = item.Precio;

            return creaInsCommand;
        }

        public override SqlCommand CreateGetByIdQuery(int id)
        {
            var byIdCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Listar_Productos",
                CommandType = CommandType.StoredProcedure
            };
            byIdCommand.Parameters.Add("p_id_producto", SqlDbType.Int).Value = id;

            return byIdCommand;
        }

        public override SqlCommand CreateDeleteQuery(int id)
        {
            var deleteCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Eliminar_Productos",
                CommandType = CommandType.StoredProcedure
            };
            deleteCommand.Parameters.Add("p_id_producto", SqlDbType.Int).Value = id;

            return deleteCommand;
        }
    }
}

