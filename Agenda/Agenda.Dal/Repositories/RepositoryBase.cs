﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Agenda.Dal.Core;
using Agenda.Dal.Helpers;

namespace Agenda.Dal.Repositories
{
    public abstract class RepositoryBase<TBo> : IMsSqlAdoRepository<TBo>
        where TBo : class, new()
    {
        public bool HasError { get; set; }
        public string ErrorMessage { get; set; }

        public virtual TBo Get(int id)
        {
            TBo registro = null;

            try
            {
                InitConnection();

                SqlDataReader reader;
                List<object[]> listaDeRegistros = new List<object[]>();

                var query = CreateGetByIdQuery(id);

                using (reader = query.ExecuteReader())
                {
                    int colCount = reader.FieldCount;

                    while (reader.Read())
                    {
                        object[] values = new object[colCount];
                        reader.GetValues(values);
                        listaDeRegistros.Add(values);
                    }

                    reader.Close();                    
                }

                if (listaDeRegistros.Count > 0)
                {
                    registro = PopulateRecords(listaDeRegistros).First();
                }
                
            }
            catch (Exception ex)
            {
                HasError = true;
                ErrorMessage = ex.Message;

            }
            finally
            {
                FinalizeConnection();
            }
            

            return registro;
        }

        public virtual IEnumerable<TBo> All()
        {
            var lista = new List<TBo>();

            try
            {
                InitConnection();
                SqlDataReader reader;
                List<object[]> listaDeRegistros = new List<object[]>();


                var query = CreateGetAllQuery();

                using (reader = query.ExecuteReader())
                {
                    int colCount = reader.FieldCount;

                    while (reader.Read())
                    {
                        object[] values = new object[colCount];
                        reader.GetValues(values);
                        listaDeRegistros.Add(values);
                    }

                    reader.Close();                    
                }

                lista = (List<TBo>) PopulateRecords(listaDeRegistros);
            }
            catch (Exception ex)
            {
                HasError = true;
                ErrorMessage = ex.Message;

            }
            finally
            {
                FinalizeConnection();
            }
         
            return lista;
        }

        public virtual int CreateOrUpdate(TBo item)
        {
            int retval = 0;

            try
            {
                InitConnection();
                var query = CreateInsertOrUpdateQuery(item);

                var sigid = new SqlParameter("@sigId", SqlDbType.Int) {Direction = ParameterDirection.ReturnValue};
                query.Parameters.Add(sigid);

                query.ExecuteNonQuery();

                retval = int.Parse(sigid.Value.ToString());
            }
            catch (Exception ex)
            {
                HasError = true;
                ErrorMessage = ex.Message;
            }
            finally
            {
                FinalizeConnection();
            }
           

            return retval;
        }

        public virtual bool Delete(int id)
        {
            bool retval = false;

            try
            {
                InitConnection();
                var query = CreateDeleteQuery(id);

                var sigid = new SqlParameter("@sigId", SqlDbType.Int) {Direction = ParameterDirection.ReturnValue};
                query.Parameters.Add(sigid);

                query.ExecuteNonQuery();

                retval = int.Parse(sigid.Value.ToString()) > 0;
            }
            catch (Exception ex)
            {
                HasError = true;
                ErrorMessage = ex.Message;
            }
            finally
            {
                FinalizeConnection();
            }
            

            return retval;
        }

        public virtual IEnumerable<TBo> PopulateRecords(List<object[]> listOfValues)
        {
            throw new NotImplementedException();
        }

        public virtual SqlCommand CreateGetAllQuery()
        {
            throw new NotImplementedException();
        }

        public virtual SqlCommand CreateInsertOrUpdateQuery(TBo item)
        {
            throw new NotImplementedException();
        }

        public virtual SqlCommand CreateGetByIdQuery(int id)
        {
            throw new NotImplementedException();
        }

        public virtual SqlCommand CreateDeleteQuery(int id)
        {
            throw new NotImplementedException();
        }

        public SqlConnection Connection { get; set; }
        public ModoEjecucion ModoEjecucion { get; set; }
        public void InitConnection()
        {
            if (ModoEjecucion == ModoEjecucion.Single)
            {
                if(Connection == null)
                    Connection = DatabaseHelper.GetConnection();

                Connection.Open();
            }            
        }

        public void FinalizeConnection()
        {
            if (ModoEjecucion == ModoEjecucion.Single)
            {
                Connection?.Close();
            }
        }
    }
}
