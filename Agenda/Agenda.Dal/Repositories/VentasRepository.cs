﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Agenda.Bo;
using Agenda.Dal.Core;

namespace Agenda.Dal.Repositories
{
    public class VentasRepository : RepositoryBase<BoVenta>
    {
        public VentasRepository()
        {
            ModoEjecucion = ModoEjecucion.Single;
        }

        public VentasRepository(SqlConnection connection)
        {
            ModoEjecucion = ModoEjecucion.UnitOfWork;
            Connection = connection;
        }


        public override IEnumerable<BoVenta> PopulateRecords(List<object[]> listOfValues)
        {
            var lista = listOfValues.Select(values => new BoVenta
            {
                NoVenta = int.Parse(values[0].ToString()),
                Fecha = DateTime.Parse(values[1].ToString()),
                Cantidad =  int.Parse(values[2].ToString()),
                Total = decimal.Parse(values[3].ToString())
            }).ToList();

            return lista;
        }

        public override SqlCommand CreateGetAllQuery()
        {
            var allCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Listar_Ventas",
                CommandType = CommandType.StoredProcedure
            };
            allCommand.Parameters.Add("p_no_venta", SqlDbType.Int).Value = 0;

            return allCommand;
        }

        public override SqlCommand CreateInsertOrUpdateQuery(BoVenta item)
        {
            var creaInsCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Crear_Actualizar_Ventas",
                CommandType = CommandType.StoredProcedure
            };
            creaInsCommand.Parameters.Add("p_no_venta", SqlDbType.Int).Value = item.NoVenta;
            creaInsCommand.Parameters.Add("p_fecha", SqlDbType.VarChar, 50).Value = item.Fecha;
            creaInsCommand.Parameters.Add("p_cantidad", SqlDbType.VarChar, 50).Value = item.Cantidad;
            creaInsCommand.Parameters.Add("p_total", SqlDbType.Float).Value = item.Total;

            return creaInsCommand;
        }

        public override SqlCommand CreateGetByIdQuery(int id)
        {
            var byIdCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Listar_Ventas",
                CommandType = CommandType.StoredProcedure
            };
            byIdCommand.Parameters.Add("p_no_venta", SqlDbType.Int).Value = id;

            return byIdCommand;
        }

        public override SqlCommand CreateDeleteQuery(int id)
        {
            var deleteCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Eliminar_Ventas",
                CommandType = CommandType.StoredProcedure
            };
            deleteCommand.Parameters.Add("p_no_venta", SqlDbType.Int).Value = id;

            return deleteCommand;
        }
    }
}
