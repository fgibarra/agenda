﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Agenda.Bo;
using Agenda.Dal.Core;

namespace Agenda.Dal.Repositories
{
    public class CitasRepository : RepositoryBase<BoCita>
    {
        
        public CitasRepository()
        {
            ModoEjecucion = ModoEjecucion.Single;
        }

        public CitasRepository(SqlConnection connection)
        {
            Connection = connection;
            ModoEjecucion = ModoEjecucion.UnitOfWork;
        }


        public override IEnumerable<BoCita> PopulateRecords(List<object[]> listOfValues)
        {
            var lista = listOfValues.Select(values => new BoCita
            {
                Id = int.Parse(values[0].ToString()),
                Evento = values[1].ToString(),
                Lugar = values[2].ToString(),
                Fecha = DateTime.Parse(values[3].ToString())
            }).ToList();

            return lista;
        }

        public override SqlCommand CreateGetAllQuery()
        {
            var allCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Listar_Citas",
                CommandType = CommandType.StoredProcedure
            };
            allCommand.Parameters.Add("p_id", SqlDbType.Int).Value = 0;

            return allCommand;
        }

        public override SqlCommand CreateInsertOrUpdateQuery(BoCita item)
        {
            var creaInsCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Crear_Actualizar_Citas",
                CommandType = CommandType.StoredProcedure
            };
            creaInsCommand.Parameters.Add("p_id", SqlDbType.Int).Value = item.Id;
            creaInsCommand.Parameters.Add("p_evento", SqlDbType.VarChar, 50).Value = item.Evento;
            creaInsCommand.Parameters.Add("p_lugar", SqlDbType.VarChar, 50).Value = item.Lugar;
            creaInsCommand.Parameters.Add("p_fecha", SqlDbType.DateTime).Value = item.Fecha;

            return creaInsCommand;
        }

        public override SqlCommand CreateGetByIdQuery(int id)
        {
            var byIdCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Listar_Citas",
                CommandType = CommandType.StoredProcedure
            };
            byIdCommand.Parameters.Add("p_id", SqlDbType.Int).Value = id;

            return byIdCommand;
        }

        public override SqlCommand CreateDeleteQuery(int id)
        {
            var deleteCommand = new SqlCommand
            {
                Connection = Connection,
                CommandText = "dbo.proc_Eliminar_Citas",
                CommandType = CommandType.StoredProcedure
            };
            deleteCommand.Parameters.Add("p_id", SqlDbType.Int).Value = id;

            return deleteCommand;
        }
    }
}
