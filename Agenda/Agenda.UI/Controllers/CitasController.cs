﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Agenda.Bll.Services;
using Agenda.Bo;

namespace Agenda.UI.Controllers
{
    public class CitasController : ApiController
    {
        public IEnumerable<BoCita> GetCitas()
        {
            var citasService = new CitasService();
            return citasService.GetCitas();
        }

        public IHttpActionResult GetCita(int id)
        {
            var citasService = new CitasService();
            var cita = citasService.GetCita(id);
            if (cita == null)
            {
                return NotFound();
            }

            return Ok(cita);
        }


        public IHttpActionResult PutCita(int id, BoCita cita)
        {
            if (ModelState.IsValid && id == cita.Id)
            {
                var citasService = new CitasService();
                var result = citasService.CreateOrUpdateCita(cita);

                if (result == 0)
                {
                    return NotFound();
                }

                return Ok(cita);
            }

            return BadRequest(ModelState);
        }

   
        public IHttpActionResult Post(BoCita cita)
        {
            if (ModelState.IsValid)
            {
                var citasService = new CitasService();
                var result = citasService.CreateOrUpdateCita(cita);

                if (result == 0)
                {
                    return NotFound();
                }

                cita.Id = result;
                var uri = new Uri(Url.Link("DefaultApi", new { id = result }));

                return Created(uri, cita);
            }

            return BadRequest(ModelState);
        }

        public IHttpActionResult Delete(int id)
        {
            var citasService =  new CitasService();
            var result = citasService.DeleteCita(id);


            if (!result)
            {
                return NotFound();
            }
            
            return Ok();
        }
    }
}
