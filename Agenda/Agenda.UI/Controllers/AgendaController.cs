﻿using System;
using System.Web.Mvc;
using Agenda.Bll.Services;
using Agenda.Bo;

namespace Agenda.UI.Controllers
{
    public class AgendaController : Controller
    {
        // GET: Agenda
        public ActionResult Index()
        {
            var citasService = new CitasService();

            return View(citasService.GetCitas());
        }

        // GET: Agenda/Details/5
        public ActionResult Details(int id)
        {
            var citasService = new CitasService();
            return View(citasService.GetCita(id));
        }

        // GET: Agenda/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Agenda/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var citasService = new CitasService();

                var cita = new BoCita
                {
                    Id = 0,
                    Evento = Request.Form["Evento"],
                    Lugar = Request.Form["Lugar"],
                    Fecha = DateTime.Parse(Request.Form["Fecha"])
                };


                if (citasService.CreateOrUpdateCita(cita) > 0)
                {
                    return RedirectToAction("Index");
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Agenda/Edit/5
        public ActionResult Edit(int id)
        {
            var citasService = new CitasService();
            return View(citasService.GetCita(id));
        }

        // POST: Agenda/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var citasService = new CitasService();
                

                var cita = new BoCita
                {
                    Id = id,
                    Evento = Request.Form["Evento"],
                    Lugar = Request.Form["Lugar"],
                    Fecha = DateTime.Parse(Request.Form["Fecha"])
                };


                if (citasService.CreateOrUpdateCita(cita) > 0)
                {
                    return RedirectToAction("Index");
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Agenda/Delete/5
        public ActionResult Delete(int id)
        {
            var citasService = new CitasService();
            return View(citasService.GetCita(id));
        }

        // POST: Agenda/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var citasService = new CitasService();

                if (citasService.DeleteCita(id))
                {
                    return RedirectToAction("Index");
                }

                return View();
            }
            catch
            {
                return View();
            }
        }
    }
}
