﻿$(document)
    .ready(function() {
        cargarDatos();
    });


function cargarDatos() {
    $.ajax({
        url: "/api/citas",
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "JSON",
        success: function(result) {
            var html = "";

            $.each(result, function(key, item) {
                html += "<tr>";
                html += "<td>" + item.Id + "</td>";
                html += "<td>" + item.Lugar + "</td>";
                html += "<td>" + item.Evento + "</td>";
                html += "<td>" + moment(item.Fecha).format('DD/MM/YYYY') + "</td>";
                html += "<td>";
                html += '<a href="#"  onclick="return traerPorId(' + item.Id + ')">Editar</a> | ';
                html += '<a href="#"  onclick="return eliminar(' + item.Id + ')">Eliminar</a>';
                html += "</td>";
                html += "</tr>";
            });

            $(".tbody").html(html);
        },
        error: function(error) {
            alert(error.responseText);
        }
    });
}

function agregar() {
    var validado = validar();

    if (validado === false) {
        return false;
    }

    var cita = {
        IdCita: $("#IdCita").val(),
        Lugar: $("#Lugar").val(),
        Evento: $("#Evento").val(),
        Fecha: $("#Fecha").val()
    };

    $.ajax({
        url: "/api/citas",
        data: JSON.stringify(cita),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "JSON",      
        success: function() {
            cargarDatos();
            $("#modalAgenda").modal('hide');
        },
        error: function(error) {
            alert(error.responseText);
        }
    });

    return true;
}


function traerPorId(id) {
    //Reseteando los controles del modal
    $("#IdCita").css("border-color", "lightgray");
    $("#Evento").css("border-color", "lightgray");
    $("#Lugar").css("border-color", "lightgray");
    $("#Fecha").css("border-color", "lightgray");

    //Traer los datos de la cita
    $.ajax({
        url: "/api/citas/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "JSON",
        statusCode : {
            200: function(cita) {
                $('#IdCita').val(cita.Id);
                $('#Evento').val(cita.Evento);
                $('#Lugar').val(cita.Lugar);
                $('#Fecha').val(moment(cita.Fecha).format('DD/MM/YYYY'));
                
                $('#modalAgenda').modal('show');
                $('#btnUpdate').show();
                $('#btnAdd').hide();
            },
            404: function() {
                alert("La cita no se encontro");
            }            
        }        
    });
}

function actualizar() {
    var validado = validar();

    if (validado === false) {
        return false;
    }

    var cita = {
        Id: $("#IdCita").val(),
        Lugar: $("#Lugar").val(),
        Evento: $("#Evento").val(),
        Fecha: $("#Fecha").val()
    };

    $.ajax({
        url: "/api/citas/" + cita.Id,
        data: JSON.stringify(cita),
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        dataType: "JSON",
        success: function () {
            cargarDatos();
            $("#modalAgenda").modal('hide');
            $("#IdCita").val("");
            $("#Lugar").val("");
            $("#Evento").val("");
            $("#Fecha").val("");
        },
        error: function (error) {
            alert(error.responseText);
        }
    });

    return true;
}

function eliminar(id) {
    var ans = confirm("¿Estas seguro que deseas eliminar este registro?");
    if (ans) {
        $.ajax({
            url: "/api/citas/" + id,
            type: "DELETE",
            contentType: "application/x-www-form-urlencoded",            
            success: function (result) {
                cargarDatos();
            },
            error: function (errormessage) {
                alert(errormessage.responseText);
            }
        });
    }
}

function validar() {
    var esValido = true;

    esValido = validarNoVacio("Lugar");
    esValido = validarNoVacio("Evento");
    esValido = validarNoVacio("Fecha");
    
    return esValido;
}

function validarNoVacio(elemento) {
    var valido = true;

    if ($('#' + elemento).val().trim() == "") {
        $('#' + elemento).css('border-color', 'Red');
        valido = false;
    } else {
        $('#' + elemento).css('border-color', 'lightgray');
    }

    return valido;
}

function clearTextBox() {
    $('#IdCita').val("");
    $('#Lugar').val("");
    $('#Evento').val("");
    $('#Fecha').val("");    
    $('#btnUpdate').hide();
    $('#btnAdd').show();
    $('#IdCita').css('border-color', 'lightgrey');
    $('#Lugar').css('border-color', 'lightgrey');
    $('#Evento').css('border-color', 'lightgrey');
    $('#Fecha').css('border-color', 'lightgrey');
}