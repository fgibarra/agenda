﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Bo
{
    public class BoCita
    {
        public int Id { get; set; }
        public string Evento { get; set; }
        public string Lugar { get; set; }

        
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

        public DateTime? Fecha { get; set; }

    }
}
