﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.Bo
{
    public class BoVentaProducto
    {
        public int NoVenta { get; set; }
        public int IdProducto { get; set; }

        public int Cantidad { get; set; }
    }
}
