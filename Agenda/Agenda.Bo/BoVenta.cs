﻿using System;

namespace Agenda.Bo
{
    public class BoVenta
    {
        public int NoVenta { get; set; }
        public DateTime Fecha { get; set; }
        public int Cantidad { get; set; }
        public decimal Total { get; set; }


    }
}
