﻿GO
    IF EXISTS(SELECT NAME FROM SYSOBJECTS(NOLOCK) WHERE NAME = 'proc_Crear_Actualizar_Citas')
       DROP PROCEDURE [dbo].[proc_Crear_Actualizar_Citas]
GO

CREATE PROCEDURE [dbo].[proc_Crear_Actualizar_Citas]
	@p_id INT,
	@p_evento VARCHAR(50),
	@p_lugar VARCHAR(50),
	@p_fecha DATETIME
AS
BEGIN
	DECLARE @sigId INT

	SET NOCOUNT ON
	 
	IF @p_id = 0 
		BEGIN
			INSERT INTO [dbo].[Citas](
				evento
				, lugar
				, fecha)
			VALUES(
				@p_evento
				, @p_lugar
				, @p_fecha)
				
			SET @sigId = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
			UPDATE [dbo].[Citas]
				SET evento = @p_evento
					, lugar = @p_lugar
					, fecha = @p_fecha
			WHERE id = @p_id
			
			SET @sigId = @p_id
		END
	
	RETURN @sigId
END