﻿GO
    IF EXISTS(SELECT NAME FROM SYSOBJECTS(NOLOCK) WHERE NAME = 'proc_Listar_Citas')
       DROP PROCEDURE [dbo].proc_Listar_Citas
GO

CREATE PROCEDURE [dbo].[proc_Listar_Citas]
	@p_id int
AS
BEGIN

	SET NOCOUNT ON
	 
	IF @p_id = 0
		BEGIN
			SELECT * FROM [dbo].[Citas]
		END
	ELSE
		BEGIN
			SELECT * FROM [dbo].[Citas]
			WHERE [id] = @p_id
		END
END
GO