﻿CREATE TABLE [dbo].[Citas] (
	[id] int NOT NULL IDENTITY(1,1), 
	[evento] varchar(50) NOT NULL, 
	[lugar] varchar(50), 
	[fecha] datetime
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Productos] (
	[IdProducto] int NOT NULL IDENTITY(1,1) PRIMARY KEY, 
	[Nombre] varchar(50) NOT NULL, 
	[Descripcion] varchar(50) NOT NULL, 
	[Precio] float NOT NULL, 
	[Activo] bit NOT NULL DEFAULT ((1))
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Ventas] (
	[NoVenta] int NOT NULL IDENTITY(1,1) PRIMARY KEY, 
	[Fecha] datetime NOT NULL, 
	[Cantidad] int NOT NULL, 
	[Total] float NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Ventas_Productos] (
	[NoVenta] int NOT NULL, 
	[IdProducto] int NOT NULL, 
	[Cantidad] int NOT NULL,
	CONSTRAINT [PK_Venta_Producto] PRIMARY KEY ([NoVenta], [IdProducto])
) ON [PRIMARY]
GO

CREATE PROCEDURE [dbo].[proc_Crear_Actualizar_Citas] @p_id INT,
	@p_evento VARCHAR(50),
	@p_lugar VARCHAR(50),
	@p_fecha DATETIME
AS
BEGIN
	DECLARE @sigId INT

	SET NOCOUNT ON
	 
	IF @p_id = 0 
		BEGIN
			INSERT INTO [dbo].[Citas](
				evento
				, lugar
				, fecha)
			VALUES(
				@p_evento
				, @p_lugar
				, @p_fecha)
				
			SET @sigId = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
			UPDATE [dbo].[Citas]
				SET evento = @p_evento
					, lugar = @p_lugar
					, fecha = @p_fecha
			WHERE id = @p_id
			
			SET @sigId = @p_id
		END
	
	RETURN @sigId
END
GO

CREATE PROCEDURE [dbo].[proc_Crear_Actualizar_Productos] @p_id_producto INT,	
	@p_nombre varchar(50),
	@p_descripcion varchar(50),
	@p_precio FLOAT
AS
BEGIN
	DECLARE @sigId INT

	SET NOCOUNT ON
	 
	IF @p_id_producto = 0 
		BEGIN
			INSERT INTO [dbo].[Productos](
				Nombre
				, Descripcion
				, Precio)
			VALUES(
				@p_nombre
				, @p_descripcion
				, @p_precio)
				
			SET @sigId = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
			UPDATE [dbo].[Productos]
				SET Nombre = @p_nombre
					, Descripcion = @p_descripcion
					,Precio = @p_precio
			WHERE IdProducto = @p_id_producto
			
			SET @sigId = @p_id_producto
		END
	
	RETURN @sigId
END
GO

CREATE PROCEDURE [dbo].[proc_Crear_Actualizar_Ventas] @p_no_venta INT,	
	@p_fecha DATETIME,
	@p_cantidad INT,
	@p_total FLOAT
AS
BEGIN
	DECLARE @sigId INT

	SET NOCOUNT ON
	 
	IF @p_no_venta = 0 
		BEGIN
			INSERT INTO [dbo].[Ventas](
				Fecha
				, Cantidad
				, Total)
			VALUES(
				@p_fecha
				, @p_cantidad
				, @p_total)
				
			SET @sigId = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
			UPDATE [dbo].[Ventas]
				SET Fecha = @p_fecha
					, Cantidad = @p_cantidad
					, Total = @p_total
			WHERE NoVenta = @p_no_venta
			
			SET @sigId = @p_no_venta
		END
	
	RETURN @sigId
END
GO

CREATE PROCEDURE [dbo].[proc_Crear_Actualizar_Ventas_Productos] @p_no_venta INT,	
	@p_id_producto INT,
	@p_cantidad FLOAT	
AS
BEGIN
	DECLARE @cuantos INT
		
	SET @cuantos = (SELECT COUNT(NoVenta)
	FROM [dbo].[Ventas_Productos]
	WHERE NoVenta = @p_no_venta
	AND IdProducto  = @p_id_producto)
	 
	IF @cuantos > 0
		BEGIN
			INSERT INTO [dbo].[Ventas_Productos](
				NoVenta
				, IdProducto
				, Cantidad)
			VALUES(
				@p_no_venta
				, @p_id_producto
				, @p_cantidad)			
		END
	ELSE
		BEGIN
			UPDATE [dbo].[Ventas_Productos]
				SET Cantidad = @p_cantidad
			WHERE IdProducto = @p_id_producto
			AND NoVenta = @p_no_venta			
		END		
END
GO

CREATE PROCEDURE [dbo].[proc_Eliminar_Citas] @p_id int
AS
BEGIN

	SET NOCOUNT ON
	 
	DELETE FROM [dbo].[Citas]
	WHERE [id] = @p_id
	
	RETURN @p_id
END
GO

CREATE PROCEDURE [dbo].[proc_Eliminar_Productos] @p_id_producto int
AS
BEGIN

	SET NOCOUNT ON
	 
	UPDATE [dbo].[Productos] SET Activo = 0
	WHERE [IdProducto] = @p_id_producto;
	
	RETURN @p_id_producto
END
GO

CREATE PROCEDURE [dbo].[proc_Eliminar_Ventas] @p_no_venta int
AS
BEGIN

	SET NOCOUNT ON
	
	DELETE FROM [dbo].[Ventas_Producto]
	WHERE NoVenta = @p_no_venta
	 
	DELETE FROM [dbo].[Ventas]
	WHERE [NoVenta] = @p_no_venta;
	
	RETURN @p_no_venta
END
GO

CREATE PROCEDURE [dbo].[proc_Listar_Citas] @p_id int
AS
BEGIN

	SET NOCOUNT ON
	 
	IF @p_id = 0
		BEGIN
			SELECT * FROM [dbo].[Citas]
		END
	ELSE
		BEGIN
			SELECT * FROM [dbo].[Citas]
			WHERE [id] = @p_id
		END
END
GO

CREATE PROCEDURE [dbo].[proc_Listar_Productos] @p_id_producto int
AS
BEGIN

	SET NOCOUNT ON
	 
	IF @p_id_producto = 0
		BEGIN
			SELECT * FROM [dbo].[Productos] WHERE Activo = 1
		END
	ELSE
		BEGIN
			SELECT * FROM [dbo].[Productos]
			WHERE [IdProducto] = @p_id_producto
		END
END
GO

CREATE PROCEDURE [dbo].[proc_Listar_Ventas] @p_no_venta int
AS
BEGIN

	SET NOCOUNT ON
	 
	IF @p_no_venta = 0
		BEGIN
			SELECT * FROM [dbo].[Ventas]
		END
	ELSE
		BEGIN
			SELECT * FROM [dbo].[Ventas]
			WHERE [NoVenta] = @p_no_venta
		END
END
GO

CREATE PROCEDURE [dbo].[proc_Listar_Ventas_Productos] @p_no_venta int
AS
BEGIN

	SET NOCOUNT ON	 	

	SELECT * FROM [dbo].[Ventas_Productos]
	WHERE [NoVenta] = @p_no_venta
		
END
GO
