﻿GO
    IF EXISTS(SELECT NAME FROM SYSOBJECTS(NOLOCK) WHERE NAME = 'proc_Eliminar_Citas')
       DROP PROCEDURE [dbo].[proc_Eliminar_Citas]
GO

CREATE PROCEDURE [dbo].[proc_Eliminar_Citas]
	@p_id int
AS
BEGIN

	SET NOCOUNT ON
	 
	DELETE FROM [dbo].[Citas]
	WHERE [id] = @p_id
	
	RETURN @p_id
END
GO